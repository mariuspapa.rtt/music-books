import { Song } from "../_models/Song";

export class DummySongList {
    public static SONG_LIST: Song[] = [new Song('https://images.genius.com/9ea40fde683775d310d8cf54958536c8.770x770x1.jpg', 'SuperStar (Ft. Rodney Jerkins)', 'Lady Gaga'),
    new Song('https://mp3-get.com/wp-content/uploads/2021/05/Maria-Becerra.jpg', 'Que Mas Pues (Ft. Maria Becerra)', 'J Balvin'),
    new Song('https://direct.rhapsody.com/imageserver/images/alb.531161126/600x600.jpg', 'Love Not War (The Tampa Beat)', 'Jason Derulo'),
    new Song('https://hiphop-n-more.com/wp-content/uploads/2020/01/justin-bieber-changes.jpeg', 'Intentions', 'Justin Bieber'),
    new Song('https://images-na.ssl-images-amazon.com/images/I/71V-x8ozIZL._AC_SL1500_.jpg', 'Positions', 'Ariana Grande'),
    new Song('https://images.genius.com/d2304429bd6b0f492325f1151bd21825.1000x1000x1.jpg', 'Break My Heart', 'Dua Lipa'),
    new Song('https://www.rollingstone.com/wp-content/uploads/2020/02/camila-cabello-dababy-video.jpg', 'My Oh My', 'Camila Cabello'),
    new Song('https://acapella.studio/wp-content/uploads/2021/02/Ava-Max-My-Head-My-Heart-Acapella-Instrumental.jpg', 'My Head & My Heart', 'Ava Max'),
    new Song('https://cdn2.thelineofbestfit.com/images/made/images/remote/https_cdn2.thelineofbestfit.com/media/2014/ed-sheeran-afterglow-video_1290_749.jpg', 'Afterglow', 'Ed Sheeran'),
    new Song('https://radiopotok.ru/f/i/2020/10/30/512_1604079794-cbe906.jpg', 'No Drama', 'Becky G, Ozuna'),
    new Song('https://i1.sndcdn.com/artworks-ioeYXSsCb5aKYsYr-efit2A-t500x500.jpg', 'Cry About It Later', 'Katy Perry'),
    new Song('https://i.ytimg.com/vi/yQajUawy-tI/maxresdefault.jpg', 'Higher Power', 'Coldplay'),]
}