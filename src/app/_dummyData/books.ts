import { Book } from "../_models/Book";

export class DummyBookList {
    public static BOOK_LIST: Book[] = [new Book('https://images-na.ssl-images-amazon.com/images/I/516mR2BJoUL._SX442_BO1,204,203,200_.jpg', 'The Japanese Art of the Cocktail', 'Masahiro Urushido',
        'Katana Kitten, one of the world’s most prominent and acclaimed Japanese cocktail bars, was opened in 2018 by highly-respected and award-winning mixologist Masahiro Urushido. Just one year later, the bar won 2019 Tales of the Cocktail Spirited Award for Best New American Cocktail Bar. Before Katana Kitten, Urushido honed his craft over several years behind the bar of award-winning eatery Saxon+Parole. In The Japanese Art of the Cocktail, Urushido shares his immense knowledge of Japanese cocktails with eighty recipes that best exemplify Japan’s contribution to the cocktail scene, both from his own bar and from Japanese mixologists worldwide. Urushido delves into what exactly constitutes the Japanese approach to cocktails, and demystifies the techniques that have been handed down over generations, all captured in stunning photography.'),
    new Book('https://images-na.ssl-images-amazon.com/images/I/51NWb-zMD7L._SY291_BO1,204,203,200_QL40_FMwebp_.jpg', 'The Blacktongue Thief (Blacktongue, 1)', 'Christopher Buehlman',
        "Set in a world of goblin wars, stag-sized battle ravens, and assassins who kill with deadly tattoos, Christopher Buehlman's The Blacktongue Thief begins a 'dazzling' (Robin Hobb) fantasy adventure unlike any other."),
    new Book('https://images-na.ssl-images-amazon.com/images/I/51Ifl1zXhJL._SX329_BO1,204,203,200_.jpg', 'The Midnight Library: A Novel', 'Matt Haig',
        "Somewhere out beyond the edge of the universe there is a library that contains an infinite number of books, each one the story of another reality. One tells the story of your life as it is, along with another book for the other life you could have lived if you had made a different choice at any point in your life. While we all wonder how our lives might have been, what if you had the chance to go to the library and see for yourself? Would any of these other lives truly be better?"),
    new Book('https://images-na.ssl-images-amazon.com/images/I/51b-JoV-1xS._SX329_BO1,204,203,200_.jpg', 'The Last Thing He Told Me: A Novel', 'Laura Dave',
        "Before Owen Michaels disappears, he smuggles a note to his beloved wife of one year: Protect her. Despite her confusion and fear, Hannah Hall knows exactly to whom the note refers—Owen’s sixteen-year-old daughter, Bailey. Bailey, who lost her mother tragically as a child. Bailey, who wants absolutely nothing to do with her new stepmother."),
    new Book('https://m.media-amazon.com/images/I/41JkOSqjeIS.jpg', 'Golden Girl', 'Elin Hilderbrand',
        "When Vivian Howe, author of thirteen novels and mother of three grown-up children, is killed in a hit-and-run incident while jogging near her home, she ascends to the Beyond. Because her death was unfair,"),
    new Book('https://m.media-amazon.com/images/I/41ZLnNiBpJL.jpg', 'The Maidens', 'Alex Michaelides',
        "For Mariana Andros - a group therapist struggling through her private grief - it's where she met her late husband. For her niece, Zoe, it's the tragic scene of her best friend's murder.As memory and mystery entangle Mariana, she finds a society full of secrets, which has been shocked to its core by the murder of one of its own."),
    new Book('https://m.media-amazon.com/images/I/41UP+d-oHAL.jpg', 'Songs in Ursa Major: A novel', 'Emma Brodie',
        "A transporting love story of music, stardom, heartbreak, and a gifted young singer-songwriter who must find her own voice—“pure sun-soaked summer fun” (Kate Quinn, bestselling author of The Alice Network)."),

    new Book('https://m.media-amazon.com/images/I/41U6zB8RCLL.jpg', 'One Two Three: A Novel', 'Laurie Frankel',
        "Somewhere out beyond the edge of the universe there is a library that contains an infinite number of books, each one the story of another reality. One tells the story of your life as it is, along with another book for the other life you could have lived if you had made a different choice at any point in your life. While we all wonder how our lives might have been, what if you had the chance to go to the library and see for yourself? Would any of these other lives truly be better?"),

    new Book('https://m.media-amazon.com/images/I/41OFl6AFT6L.jpg', 'Apples Never Fall', 'Liane Moriarty',
        "Before Owen Michaels disappears, he smuggles a note to his beloved wife of one year: Protect her. Despite her confusion and fear, Hannah Hall knows exactly to whom the note refers—Owen’s sixteen-year-old daughter, Bailey. Bailey, who lost her mother tragically as a child. Bailey, who wants absolutely nothing to do with her new stepmother."),

    new Book('https://m.media-amazon.com/images/I/51zMKWZCRfS.jpg', 'The Good Sister: A Novel', 'Sally Hepworth',
        "When Vivian Howe, author of thirteen novels and mother of three grown-up children, is killed in a hit-and-run incident while jogging near her home, she ascends to the Beyond. Because her death was unfair,"),

    new Book('https://m.media-amazon.com/images/I/41tRAkGA5oS.jpg', 'That Summer: A Novel', 'Jennifer Weiner',
        "For Mariana Andros - a group therapist struggling through her private grief - it's where she met her late husband. For her niece, Zoe, it's the tragic scene of her best friend's murder.As memory and mystery entangle Mariana, she finds a society full of secrets, which has been shocked to its core by the murder of one of its own."),
    new Book('https://m.media-amazon.com/images/I/41pKH6tZMkS.jpg', 'The Newcomer: A Novel', 'Mary Kay Andrews',
        "A transporting love story of music, stardom, heartbreak, and a gifted young singer-songwriter who must find her own voice—“pure sun-soaked summer fun” (Kate Quinn, bestselling author of The Alice Network)."),
    ]
}