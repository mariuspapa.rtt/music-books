export class Song {
    image: string;
    title: string;
    artist: string;

    constructor(image: string, title: string, artist: string) {
        this.artist = artist;
        this.image = image;
        this.title = title
    }
}