export class Book {
    image: string;
    title: string;
    author: string;
    description: string;

    constructor(image: string, title: string, author: string, description) {
        this.author = author;
        this.image = image;
        this.title = title;
        this.description = description;
    }
}