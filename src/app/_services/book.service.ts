import { Injectable } from '@angular/core';
import { DummyBookList } from '../_dummyData/books';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor() { }


  getAllBooks() {
    return DummyBookList.BOOK_LIST;
  }

  getNewReleaseBooks() {
    return DummyBookList.BOOK_LIST.slice(0, 5)
  }

  searchBooks(tags: string) {
    return DummyBookList.BOOK_LIST.filter(book => {
      return book.title.toLowerCase().includes(tags) || book.author.toLowerCase().includes(tags);
    });
  }
}
