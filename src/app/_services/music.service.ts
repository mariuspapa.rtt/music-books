import { Injectable } from '@angular/core';
import { DummySongList } from '../_dummyData/songs';

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  constructor() { }

  getAllSongs() {
    return DummySongList.SONG_LIST
  }

  getNewReleaseSongs() {
    return DummySongList.SONG_LIST.slice(0, 4)
  }

  searchSongs(tags: string) {
    return DummySongList.SONG_LIST.filter(song => {
      return song.title.toLowerCase().includes(tags) || song.artist.toLowerCase().includes(tags);
    });
  }
}
