import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookListComponent } from './components/books/book-list/book-list.component';
import { HomeComponent } from './components/home/home.component';
import { SongListComponent } from './components/music/song-list/song-list.component';
import { NavigationComponent } from './components/navigation/navigation.component';

const routes: Routes = [
  {
    path: '', component: NavigationComponent, children: [
      { path: '', component: HomeComponent },
      { path: 'songs', component: SongListComponent },
      { path: 'books', component: BookListComponent }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
