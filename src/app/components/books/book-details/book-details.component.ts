import { Component, Input, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Book } from 'src/app/_models/Book';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  @Input() book: Book;

  constructor(private dialogRef: MatDialogRef<BookDetailsComponent>) {
    this.dialogRef.disableClose = true;
    this.dialogRef.addPanelClass('book-details-dialog');
  }

  ngOnInit(): void {
  }

  closeDialog() {
    this.dialogRef.close(null);
  }

}
