import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { DummyBookList } from 'src/app/_dummyData/books';
import { Book } from 'src/app/_models/Book';
import { BookDetailsComponent } from '../book-details/book-details.component';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { BookService } from 'src/app/_services/book.service';
@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit, OnDestroy {

  books: Book[] = [];
  routeSub: Subscription;

  constructor(private dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private bookService: BookService) { }
  ngOnDestroy(): void {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.books = this.bookService.getAllBooks();
    this.filterIfQueryParams();
  }

  searchByTags(tags) {
    if (tags) {
      console.log(tags)
      this.books = this.bookService.searchBooks(tags)
      const url = this.router.createUrlTree([], {
        relativeTo: this.route,
        queryParams: {
          search: tags
        }
      }).toString();
      this.location.go(url);
    } else {
      this.books = this.bookService.getAllBooks();
      const url = this.router.createUrlTree([], {
        relativeTo: this.route,

      }).toString();
      this.location.go(url);
    }
  }

  viewBookDetails(index: number) {

    const dialodRef = this.dialog.open(BookDetailsComponent, { width: '50%' });
    dialodRef.componentInstance.book = this.books[index];

  }
  private filterIfQueryParams(): void {
    this.routeSub = this.route.queryParams.subscribe(params => {
      if (params.search) {
        this.books = this.books.filter(book => {
          return book.title.toLowerCase().includes(params.search) || book.author.toLowerCase().includes(params.search);
        });
      }
    });
  }
}
