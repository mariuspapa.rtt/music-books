import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {


  valueWasTyped!: boolean;

  @Output() searchCriteria: EventEmitter<string> = new EventEmitter();

  tags: Set<string> = new Set();

  searchForm!: FormGroup;
  constructor() { }

  ngOnInit(): void {
    this.searchForm = new FormGroup({
      searchInp: new FormControl(null)
    });
  }

  resetForm(): void {
    this.searchForm.reset();
    if (this.valueWasTyped) {
      this.searchCriteria.emit('');
    }
    this.valueWasTyped = false;
  }
  add(value: string): void {
    // Add tag
    if (value) {
      this.valueWasTyped = true;
      let values = value.split(' ');
      values = values.filter(val => {
        return val.trim() !== '';
      });
      this.tags = new Set(values);
      if (this.tags.size) {
        this.searchCriteria.emit(this.prepareTagsArgsForSearch(this.tags));
      }
    }

  }

  checkSearchInput(): void {
    if (this.valueWasTyped) {
      if (!this.searchForm.controls.searchInp.value) {
        this.searchCriteria.emit('');
        this.valueWasTyped = false;
      }
    }
  }


  private prepareTagsArgsForSearch(set: Set<string>): string {
    let concatTags = '';
    set.forEach(s => {
      concatTags += s.toLowerCase() + ' ';
    });
    return concatTags.substring(0, concatTags.length - 1);
  }

}
