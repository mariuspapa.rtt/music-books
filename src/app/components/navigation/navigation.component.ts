import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Observable, Subscription, } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { LoaderService } from 'src/app/_services/loader.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit, OnDestroy, AfterViewChecked {

  @ViewChild('drawer') drawer!: MatSidenav;

  isLoading = false;
  loaderSubs: Subscription = new Subscription;
  isMobile = false

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => {
        this.isMobile = result.matches;
        return result.matches;
      }),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private loaderService: LoaderService,
    private cdRef: ChangeDetectorRef) { }




  ngAfterViewChecked(): void {
    this.cdRef.detectChanges();
  }


  ngOnInit(): void {
    this.getIsLoading();

  }

  ngOnDestroy(): void {
    if (this.loaderSubs) {
      this.loaderSubs.unsubscribe();
    }
  }

  shouldCloseDrawer() {
    if (this.isMobile) {
      this.drawer.close();
    }
  }

  private getIsLoading() {
    this.loaderSubs = this.loaderService.isLoading$.subscribe(val => {
      this.isLoading = val;
    })
  }

}
