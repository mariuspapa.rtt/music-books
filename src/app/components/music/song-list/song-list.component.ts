import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Song } from 'src/app/_models/Song';
import { Location } from '@angular/common';
import { MusicService } from 'src/app/_services/music.service';

@Component({
  selector: 'app-song-list',
  templateUrl: './song-list.component.html',
  styleUrls: ['./song-list.component.css']
})
export class SongListComponent implements OnInit {

  songs: Song[] = [];
  songIndex = -1;
  constructor(private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private musicService: MusicService) { }

  ngOnInit(): void {
    this.songs = this.musicService.getAllSongs();
    this.filterIfQueryParams()

  }

  searchByTags(tags: string) {
    if (tags) {
      console.log(tags)
      this.songs = this.musicService.searchSongs(tags);
      const url = this.router.createUrlTree([], {
        relativeTo: this.route,
        queryParams: {
          search: tags
        }
      }).toString();
      this.location.go(url);
    } else {
      this.songs = this.musicService.getAllSongs();
      const url = this.router.createUrlTree([], {
        relativeTo: this.route,
      }).toString();
      this.location.go(url);
    }
  }
  private filterIfQueryParams(): void {
    this.route.queryParams.subscribe(params => {
      if (params.search) {
        this.songs = this.songs.filter(song => {
          return song.title.toLowerCase().includes(params.search) || song.artist.toLowerCase().includes(params.search);
        });
      }
    });
  }

  selectedIndex(i: number) {
    this.songIndex = i;
  }

}
