import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from 'src/app/_models/Book';
import { Song } from 'src/app/_models/Song';
import { BookService } from 'src/app/_services/book.service';
import { MusicService } from 'src/app/_services/music.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  books: Book[] = [];
  songs: Song[] = [];
  searchMode = 'all'
  constructor(private musicService: MusicService,
    private bookService: BookService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location) { }

  ngOnInit(): void {
    this.books = this.bookService.getNewReleaseBooks();
    this.songs = this.musicService.getNewReleaseSongs();
    this.filterIfQueryParams()
  }

  searchByTags(tags: string) {
    if (tags) {
      this.searchHelper(tags);
      const url = this.router.createUrlTree([], {
        relativeTo: this.route,
        queryParams: {
          search: tags
        }
      }).toString();
      this.location.go(url);

    } else {
      this.books = this.bookService.getNewReleaseBooks();
      this.songs = this.musicService.getNewReleaseSongs();
      const url = this.router.createUrlTree([], {
        relativeTo: this.route,
      }).toString();
      this.location.go(url);
    }


  }

  setSearchMode($event: Event) {
    this.searchMode = ($event.target as HTMLSelectElement).value;
  }

  private searchHelper(tags: string) {
    switch (this.searchMode) {
      case 'all':

        this.books = this.bookService.searchBooks(tags);
        this.songs = this.musicService.searchSongs(tags);
        break;

      case 'music':
        this.songs = this.musicService.searchSongs(tags);
        break;

      case 'books':
        this.books = this.bookService.searchBooks(tags);
        break;

    }
  }

  private filterIfQueryParams(): void {
    this.route.queryParams.subscribe(params => {
      if (params.search) {
        this.searchByTags(params.search);
      }
    });
  }


}
