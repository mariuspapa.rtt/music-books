import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './_modules/material.module';
import { SongListComponent } from './components/music/song-list/song-list.component';
import { BookListComponent } from './components/books/book-list/book-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SongComponent } from './components/music/song/song.component';
import { SearchBarComponent } from './components/_shared/search-bar/search-bar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BookComponent } from './components/books/book/book.component';
import { BookDetailsComponent } from './components/books/book-details/book-details.component';
import { HomeComponent } from './components/home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    SongListComponent,
    BookListComponent,
    SongComponent,
    SearchBarComponent,
    BookComponent,
    BookDetailsComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    NgbModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
